# Proj0-Hello
-------------

Author: Alex Marozick
Contact address: amarozic@uoregon.edu
Description: This software prints out "Hello world" and that message can be modified in the credentials.ini file.

Trivial project to exercise version control, turn-in, and other mechanisms.
